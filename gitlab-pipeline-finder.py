#!/usr/bin/env python3
import argparse
import logging
import os

import gitlab

JOB_NAME = 'release'
PROJECT_ID = 31943642

def find_pipelines_with_job(gl, project_id, job_name):
    page_limit=50
    print(f'Searching the most recent {page_limit} pipelines...')
    project = gl.projects.get(project_id)
    pipelines = project.pipelines.list(per_page=page_limit)

    for pipeline in pipelines:
        logging.debug(f'Pipelines retrieveed: {pipelines}')
        bridges = pipeline.bridges.list()
        logging.debug(f'Bridges retrieved: {bridges}')
        if bridges:
            logging.debug(f'Bridges are type: {type(bridges[0])}')
            logging.debug(f'Bridges have the following attributes and methods: {dir(bridges[0])}')
            for bridge in bridges:
                logging.debug(f'pipeline_id is {bridge.pipeline_id}')
                if bridge.downstream_pipeline:
                    logging.debug(f'downstream_pipeline is {bridge.downstream_pipeline["id"]}')

                    ds_pipeline = project.pipelines.get(id=bridge.downstream_pipeline["id"])
                    jobs = ds_pipeline.jobs.list()
                    for job in jobs:
                        logging.debug(f"Job name {job.name}")
                        if job.name == job_name:
                            print(f'Found job called {job_name} in pipeline {bridge.downstream_pipeline["id"]}, child pipeline of {pipeline.id}')
                            print(f'    Job url: https://gitlab.com/virginmediaO2/challenger/frontend/-/jobs/{job.id}')
                            print(f'    Downstream pipeline url: https://gitlab.com/virginmediaO2/challenger/frontend/-/pipelines/{bridge.downstream_pipeline["id"]}')
                            print(f'    Parent pipeline url: https://gitlab.com/virginmediaO2/challenger/frontend/-/pipelines/{pipeline.id}')


if __name__ == "__main__":
    token = os.getenv("GITLAB_TOKEN")
    if not token:
        print("Please export GITLAB_TOKEN and run me again.")
        exit(1)

    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.WARN)

    gl = gitlab.Gitlab(private_token=token)
    print(f'Searching for a job called {JOB_NAME} in project id {PROJECT_ID}...')
    find_pipelines_with_job(gl=gl, project_id=PROJECT_ID, job_name=JOB_NAME)
