# Gitlab Pipeline Finder

Find GitLab Pipelines which contained a specific Job using Python.

## Requirements

* Python3
* Internet connection
* GitLab Personal Access Token with api authorisation

## Installing

```bash
python3 -m venv ./venv
source ./venv/bin/activate
pip3 install -r requirements.txt
```

## Running

```bash
export GITLAB_TOKEN=<personal_access_token>
./gitlab-pipeline-finder.py 
```

Run with `-v` or `--verbose` to output debug information.
